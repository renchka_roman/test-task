package ua.com.ren4ka.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import ua.com.ren4ka.bean.Degree;
import ua.com.ren4ka.model.UniversityModel;
import ua.com.ren4ka.view.UniversityView;

import static ua.com.ren4ka.bean.Degree.*;

@Component
public class UniversityControllerImpl implements UniversityController {

  private UniversityModel model;
  private UniversityView view;

  @Override
  @Autowired
  public void setModel(UniversityModel model) {
    this.model = model;
  }

  @Override
  @Autowired
  public void setView(UniversityView view) {
    this.view = view;
  }

  public void getAnswer(String question) {
    int questionNumber = model.identifyQuestionNumber(question);
    switch (questionNumber) {
      case 0:
        view.printAnswerNotFound();
        break;
      case 1:
        String[] parameters = model.getNameOfDepartmentHead(question);
        view.printHeadOfDepartment(parameters[0], parameters[1]);
        break;
      case 2:
        Map<Degree, Integer> statistic =
            model.getDepartmentStatistic(question);
        view.printDepartmentStatistic(statistic.get(ASSISTANT),
            statistic.get(ASSOCIATE_PROFESSOR), statistic.get(PROFESSOR));
        break;
      case 3:
        Map<String, Double> averageSalary =
            model.getAverageSalaryForDepartment(question);
        String departmentName = (String) averageSalary.keySet().toArray()[0];
        view.printAverageDepartmentSalary(
            departmentName, averageSalary.get(departmentName));
        break;
      case 4:
        view.printCountOfDepartmentEmployee(
            model.getCountOfEmployeeForDepartment(question));
        break;
      case 5:
        view.printGlobalSearchResult(model.globalSearch(question));
        break;
    }
  }
}
