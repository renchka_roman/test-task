package ua.com.ren4ka.controller;

import ua.com.ren4ka.model.UniversityModel;
import ua.com.ren4ka.view.UniversityView;

public interface UniversityController {
  void getAnswer(String question);
  void setModel(UniversityModel model);
  void setView(UniversityView view);
}
