package ua.com.ren4ka.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.com.ren4ka.bean.Degree;
import ua.com.ren4ka.bean.Lector;

@Repository
public class UniversityDaoImpl implements UniversityDao {

  private JdbcTemplate jdbcTemplate;

  @Autowired
  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public String getNameOfDepartmentHead(String departmentName) {
    try {
      String sql =
          "SELECT name FROM departments,university_employees " +
              "WHERE department_name = ? AND head_id = university_employees.id";
      return jdbcTemplate.queryForObject(sql, String.class, departmentName);
    } catch (EmptyResultDataAccessException e) {
      return "";
    }
  }

  @Override
  public List<Lector> getAllDepartmentLectors(String departmentName) {
    String departmentEmployeesTable = getTableNameOfDepartmentEmployees(departmentName);
    if (departmentEmployeesTable == null || departmentEmployeesTable.equals("")) {
      return new ArrayList<>();
    }
    String sql =
        "SELECT name, degree, salary FROM university_employees AS un_empl," +
            " " + departmentEmployeesTable + " " +
            "WHERE un_empl.id = university_employee_id " +
            "AND degree IS NOT NULL";
    return jdbcTemplate.query(sql, this::mapLector);
  }

  @Override
  public double getAverageSalaryForDepartment(String departmentName) {
    String departmentEmployeesTable = getTableNameOfDepartmentEmployees(departmentName);
    if (departmentEmployeesTable == null) {
      return 0;
    }
    String sql =
        "SELECT AVG(salary) FROM university_employees AS un_empl," +
            " " + departmentEmployeesTable + " " +
            "WHERE un_empl.id = university_employee_id";
    Double averageSalary =
        jdbcTemplate.queryForObject(sql, Double.class);
    return averageSalary == null ? 0 : averageSalary;
  }

  @Override
  public int getEmployeeCount(String departmentName) {
    String departmentEmployeesTable = getTableNameOfDepartmentEmployees(departmentName);
    if (departmentEmployeesTable == null) {
      return 0;
    }
    String sql =
        "SELECT COUNT(*) FROM university_employees AS un_empl," +
            " " + departmentEmployeesTable + " " +
            "WHERE un_empl.id = university_employee_id";
    Integer employeeCount =
        jdbcTemplate.queryForObject(sql, Integer.class);
    return employeeCount == null ? 0 : employeeCount;
  }

  @Override
  public List<String> globalSearch(String template) {
    String sql =
        "SELECT name FROM university_employees " +
            "WHERE name LIKE ?";
    String regTemplate = "%" + template + "%";
    return jdbcTemplate.query(sql, this::mapString, regTemplate);
  }

  private String getTableNameOfDepartmentEmployees(String departmentName) {
    try {
      String sql =
          "SELECT employee_table_name FROM departments " +
              "WHERE department_name = ?";
      return jdbcTemplate.queryForObject(sql, String.class, departmentName);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private Lector mapLector(ResultSet resultSet, int i) throws SQLException {
    return new Lector(
        resultSet.getString("name"),
        resultSet.getInt("salary"),
        Degree.valueOf(resultSet.getString("degree"))
    );
  }

  private String mapString(ResultSet resultSet, int i) throws SQLException {
    return resultSet.getString("name");
  }
}
