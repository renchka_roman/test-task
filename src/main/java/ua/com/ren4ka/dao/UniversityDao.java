package ua.com.ren4ka.dao;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.com.ren4ka.bean.Lector;

public interface UniversityDao {

  String getNameOfDepartmentHead(String departmentName);

  List<Lector> getAllDepartmentLectors(String departmentName);

  double getAverageSalaryForDepartment(String departmentName);

  int getEmployeeCount(String departmentName);

  List<String> globalSearch(String template);

  void setJdbcTemplate(JdbcTemplate jdbcTemplate);
}
