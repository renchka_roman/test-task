package ua.com.ren4ka.model;

import java.util.List;
import java.util.Map;
import ua.com.ren4ka.bean.Degree;
import ua.com.ren4ka.dao.UniversityDao;

public interface UniversityModel {

  int identifyQuestionNumber(String question);

  String[] getNameOfDepartmentHead(String question);

  Map<Degree, Integer> getDepartmentStatistic(String question);

  Map<String, Double> getAverageSalaryForDepartment(String question);

  int getCountOfEmployeeForDepartment(String question);

  List<String> globalSearch(String question);

  void setDao(UniversityDao dao);
}
