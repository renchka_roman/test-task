package ua.com.ren4ka.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.com.ren4ka.bean.Degree;
import ua.com.ren4ka.bean.Lector;
import ua.com.ren4ka.dao.UniversityDao;

@Component
public class UniversityModelImpl implements UniversityModel {

  private UniversityDao dao;
  private static Map<Integer, String> questions = new HashMap<>();

  static {
    questions.put(1, "Who is head of department .+");
    questions.put(2, "Show .+ statistic.*");
    questions.put(3, "Show the average salary for department .+");
    questions.put(4, "Show count of employee for .+");
    questions.put(5, "Global search by .+");
  }

  @Override
  @Autowired
  public void setDao(UniversityDao dao) {
    this.dao = dao;
  }

  @Override
  public int identifyQuestionNumber(String question) {
    for (Map.Entry<Integer, String> pair : questions.entrySet()) {
      if (question.matches(pair.getValue())) {
        return pair.getKey();
      }
    }
    return 0;
  }

  @Override
  public String[] getNameOfDepartmentHead(String question) {
    int begin = "Who is head of department".length();
    int end = question.length();
    String departmentName = getWordFromStringQuestion(question, begin, end);
    String[] result = new String[2];
    result[0] = departmentName;
    result[1] = dao.getNameOfDepartmentHead(departmentName);
    return result;
  }

  @Override
  public Map<Degree, Integer> getDepartmentStatistic(String question) {
    int begin = "Show".length();
    int end = question.indexOf("statistic");
    String departmentName = getWordFromStringQuestion(question, begin, end);
    Map<Degree, Integer> statistic = new HashMap<>();
    statistic.put(Degree.ASSISTANT, 0);
    statistic.put(Degree.ASSOCIATE_PROFESSOR, 0);
    statistic.put(Degree.PROFESSOR, 0);
    for (Lector lector : dao.getAllDepartmentLectors(departmentName)) {
      Degree degree = lector.getDegree();
      switch (degree) {
        case ASSISTANT:
          statistic.put(Degree.ASSISTANT, statistic.get(Degree.ASSISTANT) + 1);
          break;
        case ASSOCIATE_PROFESSOR:
          statistic.put(Degree.ASSOCIATE_PROFESSOR, statistic.get(Degree.ASSOCIATE_PROFESSOR) + 1);
          break;
        case PROFESSOR:
          statistic.put(Degree.PROFESSOR, statistic.get(Degree.PROFESSOR) + 1);
          break;
      }
    }
    return statistic;
  }

  @Override
  public Map<String, Double> getAverageSalaryForDepartment(String question) {
    int begin = "Show the average salary for department".length();
    int end = question.length();
    String departmentName = getWordFromStringQuestion(question, begin, end);
    Map<String, Double> result = new HashMap<>();
    double average = dao.getAverageSalaryForDepartment(departmentName);
    result.put(departmentName, average);
    return result;
  }

  @Override
  public int getCountOfEmployeeForDepartment(String question) {
    int begin = "Show count of employee for".length();
    int end = question.length();
    String departmentName = getWordFromStringQuestion(question, begin, end);
    return dao.getEmployeeCount(departmentName);
  }

  @Override
  public List<String> globalSearch(String question) {
    int begin = "Global search by".length();
    int end = question.length();
    String template = getWordFromStringQuestion(question, begin, end);
    return dao.globalSearch(template);
  }

  private String getWordFromStringQuestion(String question, int begin, int end) {
    return question.substring(begin, end).trim().replaceAll("[\\.?]","");
  }
}
