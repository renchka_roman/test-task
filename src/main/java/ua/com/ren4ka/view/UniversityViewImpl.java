package ua.com.ren4ka.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.com.ren4ka.controller.UniversityController;

@Component
public class UniversityViewImpl implements UniversityView {

  private UniversityController controller;
  private static BufferedReader reader =
      new BufferedReader(new InputStreamReader(System.in));

  @Override
  @Autowired
  public void setController(UniversityController controller) {
    this.controller = controller;
  }

  @Override
  public void startConversation() {
    System.out.println("Hello. What is your question?(Q - quit)");
    String inputStr = "";
    boolean isExit = false;
    while (!isExit) {
      try {
        System.out.print("Question: ");
        inputStr = reader.readLine();
        if (!inputStr.equals("Q")) {
          controller.getAnswer(inputStr);
        } else {
          isExit = true;
          System.out.println("Good bye :)");
        }
      } catch (IOException e) {
        System.out.println("Please, enter again.");
      }
    }
  }

  @Override
  public void printHeadOfDepartment(
      String departmentName, String nameOfDepartmentHead) {
    System.out.print("Answer: ");
    if (nameOfDepartmentHead.equals("")) {
      System.out.println("Department not found");
    } else {
      System.out.println("Head of " + departmentName + " department is "
          + nameOfDepartmentHead);
    }
  }

  @Override
  public void printDepartmentStatistic(
      int assistantCount, int associateProfessorCount, int professorCount) {
    System.out.println("Answer: \n\t assistants - " + assistantCount);
    System.out.println("\t associate professors - " + associateProfessorCount);
    System.out.println("\t professors - " + professorCount);
  }

  @Override
  public void printAverageDepartmentSalary(
      String departmentName, double averageSalary) {
    System.out.println("Answer: The average salary of " +
        departmentName + " is " + averageSalary);
  }

  @Override
  public void printCountOfDepartmentEmployee(int countOfEmployee) {
    System.out.println("Answer: " + countOfEmployee);
  }

  @Override
  public void printGlobalSearchResult(List<String> searchResult) {
    if (searchResult.size() == 0) {
      System.out.println("Answer: Not found");
    } else {
      StringBuilder builder = new StringBuilder("Answer: ");
      for (String name : searchResult) {
        builder.append(name);
        builder.append(", ");
      }
      builder.delete(builder.length() - 2, builder.length());
      System.out.println(builder);
    }
  }

  @Override
  public void printAnswerNotFound() {
    System.out.println("Please enter correct question.");
  }
}
