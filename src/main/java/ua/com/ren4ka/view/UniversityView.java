package ua.com.ren4ka.view;

import java.util.List;
import ua.com.ren4ka.controller.UniversityController;

public interface UniversityView {

  void startConversation();

  void printHeadOfDepartment(String departmentName, String headOfDepartmentName);

  void printDepartmentStatistic(
      int assistantCount, int associateProfessorCount, int professorCount);

  void printAverageDepartmentSalary(String departmentName, double averageSalary);

  void printCountOfDepartmentEmployee(int countOfEmployee);

  void printGlobalSearchResult(List<String> searchResult);

  void printAnswerNotFound();

  void setController(UniversityController controller);
}
