package ua.com.ren4ka;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.com.ren4ka.config.SpringConfig;
import ua.com.ren4ka.view.UniversityView;

public class Application {

  public static void main(String[] args) {

    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
        SpringConfig.class);
    UniversityView view = context.getBean(UniversityView.class);

    view.startConversation();
  }
}
