package ua.com.ren4ka.bean;

import java.util.ArrayList;
import java.util.List;

public class Department {

  private String name;
  private Employee head;
  private List<Lector> lectorList = new ArrayList<>();

  public Department(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Employee getHead() {
    return head;
  }

  public void setHead(Employee head) {
    this.head = head;
  }

  public List<Lector> getLectorList() {
    return lectorList;
  }

  public void setLectorList(List<Lector> lectorList) {
    this.lectorList = lectorList;
  }
}
