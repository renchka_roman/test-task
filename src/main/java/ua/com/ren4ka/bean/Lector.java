package ua.com.ren4ka.bean;

public class Lector extends Employee {

  private Degree degree;

  public Lector(String name, int salary, Degree degree) {
    super(name, salary);
    this.degree = degree;
  }

  public Degree getDegree() {
    return degree;
  }

  public void setDegree(Degree degree) {
    this.degree = degree;
  }
}
