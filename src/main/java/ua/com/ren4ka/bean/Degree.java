package ua.com.ren4ka.bean;

public enum Degree {
  ASSISTANT,
  ASSOCIATE_PROFESSOR,
  PROFESSOR,
}
